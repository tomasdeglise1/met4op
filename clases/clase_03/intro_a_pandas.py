import os
# try:
# 	os.chdir(os.path.join(os.getcwd(), '../'))
# 	print(os.getcwd())
# except:
# 	pass

import numpy as np
import pandas as pd
import seaborn as sns

# Creación

# Creación de una Serie
s = pd.Series([1, 3, 5, np.nan, 6, 8])
s

# Creación de un Data-Frame (df)

# Vía Numpy
dates = pd.date_range('20130101', periods=6)
dates

df = pd.DataFrame(np.random.randn(6, 4),
                  index=dates,
                  columns=['A', 'B', 'C', 'D'])
df

# Vía diccionarios de Python

df2 = pd.DataFrame({
    'A': 1.,
    'B': pd.Timestamp('20130102'),
    'C': pd.Series(1, index=list(range(4)), dtype='float32'),
    'D': np.array([3] * 4, dtype='int32'),
    'E': pd.Categorical(["test", "train", "test", "train"]),
    'F': 'foo'
})
df2

# Tipos de Datos

df2.dtypes

# Importar datos
df_in = pd.read_csv(
    'dataset/censo2010/hogar.csv',  # file path
    delimiter=',',  # delimitador ',',';','|','\t'
    header=0,  # número de fila como nom de col
    names=None,  # nombre de las columnas (ojo con header)
    index_col=0,  # que col es el índice
    usecols=None,  # que col usar. Ej: [0, 1, 2], ['foo', 'bar', 'baz']
    dtype=None,  # Tipo de col {'a': np.int32, 'b': str} 
    skiprows=None,  # saltear fil al init
    skipfooter=0,  # saltear fil al final
    nrows=None,  # n de fil a leer
    decimal=',',  # separador de decimal. Ej: ',' para EU dat
    quotechar='"',  # char para reconocer str
    encoding=None,  # para los acentos, ñ, etc
)
df_in

df_in = pd.read_csv('dataset/censo2010/hogar.csv')
df_in

# > ver https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html

# Revisando un df
df_in.head()

df_in.tail(3)

df_in.index
type(df_in.index)

df_in.columns
type(df_in.columns)

df_in.values
type(df_in.values)

df_in.shape

# Subsetting with []

# Selecting a single column as a Series
#
# There are two main components of a Series,
# the index and the data(or values).
# There are NO columns in a Series.
# The name of the Series becomes the old-column name.
df_in['ALGUNBI']
type(df_in['ALGUNBI'])

## Selecting columns as a DF
df_in[['ALGUNBI']]
type(df_in[['ALGUNBI']])

# Select multiple columns as a DataFrame by passing a list to it:
df_in[['PROP', 'ALGUNBI']]

# change orders
df_in[['ALGUNBI', 'PROP']]

# select by number
df_in.columns[0]
df_in[df_in.columns[0]]
df_in[[df_in.columns[0]]]

# by numbers
df_in.columns
df_in[df_in.columns[[18, 21]]]

# Subsetting Rows

# x rango
# Wait a second… Isn’t [ ] just for column selection?
df_in[0:1]
df_in[0:10]

# Operator Overloading

# Just the indexing operator is overloaded.
# This means, that depending on the inputs,
# pandas will do something completely different.
# Here are the rules for the different objects you pass to just the indexing operator.
#
#    string — return a column as a Series
#    list of strings — return all those columns as a DataFrame
#    a slice — select rows (can do both label and integer location — confusing!)
#    a sequence of booleans — select all rows where True

# .loc

# The .loc indexer selects data in a different
# way than just the indexing operator.
# It can select subsets of rows or columns.
# It can also simultaneously select subsets
# of rows and columns. Most importantly, it only
# selects data by the LABEL of the rows and columns.

# to series
df_in.loc[1]
type(df_in.loc[1])

# to df
df_in.loc[[1]]
type(df_in.loc[[1]])

# many rows
df_in.loc[[1, 2]]

# many rows by it index
df_in.loc[1:10]
df_in.loc[:10]
df_in.loc[10:]

# Subsetting Rows & Cols by loc
# many rows
df_in.loc[[1, 2], ['PROP', 'ALGUNBI']]

# Selecting all of the rows and some columns
df_in.loc[:, ['PROP', 'ALGUNBI']]

# Selecting all of the rows and some columns
df_in.loc[:, 'PROP':]

# accediento al elemento
df_in.loc[1, 'PROP']
df_in.loc[:, 'PROP'][1]
df_in.loc[:, 'PROP'].loc[1]
df_in.loc[1]['PROP']
df_in.loc[1].loc['PROP']
# el elemento en forma de serie
df_in.loc[[1], ['PROP']]
type(df_in.loc[[1], ['PROP']])
# el elemento en forma de df
df_in.loc[[1], ['PROP']]
type(df_in.loc[[1], ['PROP']])

# .iloc

# The .iloc indexer is very similar to .loc
# but only uses integer locations to make its selections.
# The word .iloc itself stands for integer location
# so that should help with remember what it does

df_in.iloc[0]
df_in.iloc[[0]]
df_in.iloc[:, 18]
df_in.iloc[:, [18]]
df_in.iloc[[5, 2, 4]]
df_in.iloc[3:5]
df_in.iloc[3:5, [18, 21]]
df_in.iloc[3:5, 18:21]

# Boolean Seleccion
# Using [ ] and .loc for boolean selection
#.loc and [] work the same on a Series for boolean selection

# [ ]

# A mano
df_in_head = df_in.head()

booleano = [False, False, True, True, True]
type(booleano)
df_in_head[booleano]

booleano = np.array([False, False, True, True, True])
type(booleano)
df_in_head[booleano]

# Condiciones simples
# Usando operaciones lógicas: '<', '>', '==', '>=', '<=', !=
booleano = df_in.ALGUNBI > 0
booleano
df_in[booleano]

booleano = df_in.ALGUNBI == 0
df_in[booleano]

# Condiciones Múltiples
# Usando &, | , ~

# Usando un &
booleano = (df_in.ALGUNBI > 0) & (df_in.PROP == 1)
df_in[booleano]

# Usando un ~
booleano = ~((df_in.ALGUNBI > 0) & (df_in.PROP == 1))
df_in[booleano]

# Usando un |
df_in[ ((df_in.ALGUNBI > 0) | (df_in.PROP != 1))]

# Filtrando por ocurrencias: isin
df_in[ df_in.PROP.isin([1,2]) ]

# Buscando missing values
df_in[ df_in.PROP.isnull() ]

# Usar .loc para seleccior columnas y bools de filas
df_in.loc[ df_in.PROP.isin([1,2]), ['PROP','ALGUNBI']]

# Usar la comparación de dos columnas
# Usar .loc para seleccior columnas y bools de filas
df_in.loc[ df_in.PROP > df_in.ALGUNBI ]

# se puede usar loc para seleecionar cols con bool
# df.loc[:,bool]

# -------------------------------------------------------

# 3 funciones:

# Describime todas las cuantitativas
df_in.describe()

# Describime una univariada
df_in.ALGUNBI.value_counts()
pd.crosstab(
	df_in.ALGUNBI," ", 
	normalize='columns',
	colnames = ['']
	)

# Describime una bivariada
pd.crosstab(df_in.ALGUNBI,df_in.PROP)
pd.crosstab(df_in.ALGUNBI,df_in.PROP, normalize='index')
pd.crosstab(df_in.ALGUNBI,df_in.PROP, normalize='columns')
pd.crosstab(df_in.ALGUNBI,df_in.PROP, normalize='all')


pd.crosstab(
	df_in.ALGUNBI,
	df_in.PROP, 
	rownames = ['ALGUNBI'],
	colnames = ['PROP'],
	margins = 'all',
	margins_name = 'Total'
)


sns.heatmap(
	pd.crosstab(df_in.ALGUNBI,df_in.TOTPERS,normalize='columns'),
	cmap="YlGnBu", annot=True, cbar=False
)



# # De pd a np

# df.to_numpy()

# df2.to_numpy()

# # > DataFrame.to_numpy() does not include the index or column labels in the output.

# # Resumen estadístic

# df.describe()

# # trasponiendo un df

# df.T

# # Ordenando un df

# ## by an axis:
# df.sort_index(axis=1, ascending=False)

# ## by values
# df.sort_values(by='B')

# # Select

# df['A'][:]

# df[['A']]

# df.A
